class Book
  attr_reader :title

  def title=(new_title)
    new_title[0] = new_title[0].capitalize
    @title = new_title.split.map do |x|
      %w[the a an and in of].include?(x) ? x : x.capitalize
    end.join(' ')
  end
end
