class Timer
  attr_reader :seconds, :time_string

  def initialize
    @seconds = 0
  end

  def pad(num)
    num < 10 ? "0#{num}" : "#{num}"
  end

  def seconds=(new_seconds)
    @time_string = "#{pad(new_seconds / 3600 % 60)}:"\
      "#{pad(new_seconds / 60 % 60)}:"\
      "#{pad(new_seconds % 60)}"
  end
end
