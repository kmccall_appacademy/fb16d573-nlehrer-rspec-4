class Temperature
  attr_reader :in_fahrenheit, :in_celsius

  def initialize(opt)
    if opt.key? :f
      @in_fahrenheit = opt[:f]
      @in_celsius = (opt[:f] - 32) * 5.0 / 9
    elsif opt.key? :c
      @in_celsius = opt[:c]
      @in_fahrenheit = opt[:c] * 9.0 / 5 + 32
    end
  end

  def self.from_celsius(temp_c)
    new(c: temp_c)
  end

  def self.from_fahrenheit(temp_f)
    new(f: temp_f)
  end
end

class Celsius < Temperature
  def initialize(temp_c)
    @in_celsius = temp_c
    @in_fahrenheit = temp_c * 9.0 / 5 + 32
  end
end

class Fahrenheit < Temperature
  def initialize(temp_f)
    @in_fahrenheit = temp_f
    @in_celsius = (temp_f - 32) * 5.0 / 9
  end
end
