class Dictionary
  attr_reader :entries

  def initialize
    @entries = {}
  end

  def add(input)
    @entries.merge!(input) if input.is_a? Hash
    @entries[input] = nil if input.is_a? String
  end

  def keywords
    @entries.keys.sort
  end

  def include?(key)
    @entries.keys.include? key
  end

  def find(search)
    @entries.select { |key, _| key.match(/#{search}*/) }
  end

  def printable
    keywords.map { |key| "[#{key}] \"#{@entries[key]}\"" }.join("\n")
  end
end
